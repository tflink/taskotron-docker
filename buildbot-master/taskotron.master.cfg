# -*- python -*-
# ex: set syntax=python:

# This is a sample buildmaster config file. It must be installed as
# 'master.cfg' in your buildmaster's base directory.

# This is the dictionary that the buildmaster pays attention to. We also use
# a shorter alias to save typing.
c = BuildmasterConfig = {}

####### BUILDSLAVES

# The 'slaves' list defines the set of recognized buildslaves. Each element is
# a BuildSlave object, specifying a unique slave name and password.  The same
# slave name and password must be configured on the slave.
from buildbot.buildslave import BuildSlave
c['slaves'] = [
                BuildSlave("buildslave", "buildslave_password"),
              ]

# 'slavePortnum' defines the TCP port to listen on for connections from slaves.
# This must match the value configured into the buildslaves (with their
# --master option)
c['slavePortnum'] = 9989

####### CHANGESOURCES

# the 'change_source' setting tells the buildmaster how it should find out
# about source code changes.  Here we point to the buildbot clone of pyflakes.

#from buildbot.changes.gitpoller import GitPoller
#c['change_source'] = []
#c['change_source'].append(GitPoller(
#        'gitolite3@localhost:exampletrigger',
#        workdir='gitpoller-workdir', branch='master',
#        pollinterval=300))

#c['change_source'].append(GitPoller(
#        'git://github.com/buildbot/pyflakes.git',
#        workdir='gitpoller-workdir', branch='master',
#        pollinterval=300))
####### SCHEDULERS

# Configure the Schedulers, which decide how to react to incoming changes.  In this
# case, just kick off a 'runtests' build

from buildbot.schedulers.basic import SingleBranchScheduler
from buildbot.schedulers.forcesched import ForceScheduler, StringParameter
from buildbot.changes import filter
from buildbot.schedulers.basic import BaseBasicScheduler
from buildbot import util

class JobScheduler(BaseBasicScheduler):

    def __init__(self, name, **kwargs):
        BaseBasicScheduler.__init__(self, name, **kwargs)

    def getChangeFilter(self, branch, branches, change_filter, categories):
        return filter.ChangeFilter.fromSchedulerConstructorArgs(
                change_filter=change_filter, categories=categories)

    def getTimerNameForChange(self, changes):
        return "only"

    def getChangeClassificationsForTimer(self, objectid, timer_name):
        return self.master.db.schedulers.getChangeClassifications(
                self.objectid)


c['schedulers'] = []
c['schedulers'].append(JobScheduler(
                            name="jobsched-noarch",
                            builderNames=["all"],
                            treeStableTimer=None,
                            change_filter=filter.ChangeFilter(project='rpmcheck',
                                                                category='noarch')))

c['schedulers'].append(JobScheduler(
                            name="jobsched-i386",
                            builderNames=['i386'],
                            treeStableTimer=None,
                            change_filter=filter.ChangeFilter(project='rpmcheck',
                                                                category='i386')))

c['schedulers'].append(JobScheduler(
                            name="jobsched-x86_64",
                            builderNames=['x86_64'],
                            treeStableTimer=None,
                            change_filter=filter.ChangeFilter(project='rpmcheck',
                                                                category='x86_64')))

#c['schedulers'].append(SingleBranchScheduler(
#                            name="all",
#                            change_filter=filter.ChangeFilter(branch='master'),
#                            treeStableTimer=None,
#                            builderNames=["all", 'x86_64', 'i386']))
c['schedulers'].append(ForceScheduler(
                            name="force",
                            builderNames=["all", 'x86_64', 'i386']))
c['schedulers'].append(ForceScheduler(
                            name="rpmcheck",
                            builderNames=["all", 'x86_64', 'i386'],
                            properties=[
                            StringParameter(name='taskname',
                                            label='name of check to run',
                                            default='',
                                            size=256),
                            StringParameter(name='item',
                                            label='object to use',
                                            default='',
                                            size=256),
                            StringParameter(name='item_type',
                                            label='type of object to use',
                                            default='',
                                            size=256),
                            StringParameter(name='uuid',
                                            label='UUID of the build in progress',
                                            default='',
                                            size=256),
                            StringParameter(name='arch',
                                            label='arch of rpm to test',
                                            default='x86_64',
                                            size=128)]))

####### BUILDERS

# The 'builders' list defines the Builders, which tell Buildbot how to perform a build:
# what steps, and which slaves can execute them.  Note that any particular build will
# only take place on one slave.

from buildbot.process.factory import BuildFactory
from buildbot.steps.source.git import Git
from buildbot.steps.shell import ShellCommand
from buildbot.process.properties import Property, Interpolate
from buildbot.steps.slave import RemoveDirectory
from buildbot.steps.transfer import DirectoryUpload, FileUpload
from buildbot.steps.master import MasterShellCommand

factory = BuildFactory()

# clean out /var/tmp/taskotron (see T253)
factory.addStep(ShellCommand(command="rm -rf /var/tmp/taskotron/*", name="rm_tmp", descriptionDone=['Clean tmp']))

# clean the log (see T230)
factory.addStep(ShellCommand(command=["rm", "-f", "/var/log/taskotron/taskotron.log"], name="rm_log", descriptionDone=['Clean log']))

# check out the source
factory.addStep(Git(repourl=Property('repo', default=Interpolate('/var/lib/git/mirror/fedoraqa/%(prop:taskname)s/')), #TODO: use grokmirror?
                    branch=Property('branch', default='master'),
                    mode='full',
                    method='clobber',
                    shallow=True))

# run the runner
factory.addStep(ShellCommand(command=["runtask",
                                        '-i', Interpolate('%(prop:item)s'),
                                        '-t', Interpolate('%(prop:item_type)s'),
                                        '-a', Interpolate('%(prop:arch)s'),
                                        '-j', Interpolate('%(prop:buildername)s/%(prop:buildnumber)s'),
                                        '--local', #TODO: make this better
                                        '--uuid', Interpolate('%(prop:uuid)s'),
                                        'runtask.yml'],
                             descriptionDone=[Interpolate('%(prop:taskname)s on %(prop:item)s')],
                             name='runtask',
                             timeout=2400,
                             logfiles={'taskotron.log': {'filename': '/var/log/taskotron/taskotron-root.log', }})) #TODO: different in PROD

# create artifacts dir on master
factory.addStep(MasterShellCommand(command=["mkdir", '-m', '0755', Interpolate('/srv/taskotron/artifacts/%(prop:uuid)s')],
                                   descriptionDone=['Create artifacs dir']))

# copy artifacts to master
factory.addStep(DirectoryUpload(slavesrc=Interpolate('/var/lib/taskotron/artifacts/%(prop:uuid)s/'),
                                masterdest=Interpolate('/srv/taskotron/artifacts/%(prop:uuid)s/task_output')))

# # gzip artifacts
# factory.addStep(MasterShellCommand(command=Interpolate('gzip -r /srv/taskotron/artifacts/%(prop:uuid)s/task_output/*'),
#                                    descriptionDone=['gzip artifacs dir content']))

# copy taskotron log to master
factory.addStep(FileUpload(slavesrc='/var/log/taskotron/taskotron-root.log',
                           masterdest=Interpolate('/srv/taskotron/artifacts/%(prop:uuid)s/taskotron.log'),
                           mode=0644))

import datetime
from buildbot.process.properties import renderer

@renderer
def today(props):
    return datetime.datetime.now().strftime("%Y%m%d")

# move artifacts dir
factory.addStep(MasterShellCommand(command=Interpolate('mkdir -p -m 0755 /srv/taskotron/artifacts/%(kw:today)s && mkdir -p -m 0755 /srv/taskotron/artifacts/all && mv /srv/taskotron/artifacts/%(prop:uuid)s/ /srv/taskotron/artifacts/%(kw:today)s/ && ln -s /srv/taskotron/artifacts/%(kw:today)s/%(prop:uuid)s /srv/taskotron/artifacts/all/', today=today),
                                   descriptionDone=['Move artifacs dir']))

####### RESOURCE LOCKS
#
# This is a set of resource locks to make sure that we don't have too many things
# going on on each slave at one time.

from buildbot import locks

build_lock = locks.SlaveLock("slave_builds",
                             maxCount = 1)

####### Builders
#
# The builders associate factories with slaves which are capable of executing those factory's tasks

from buildbot.config import BuilderConfig

c['builders'] = []
c['builders'].append(
    BuilderConfig(name="x86_64",
      slavenames=["buildslave"],
      factory=factory, locks=[build_lock.access('counting')],
      mergeRequests=False))

c['builders'].append(
    BuilderConfig(name="i386",
      slavenames=["buildslave"],
      factory=factory, locks=[build_lock.access('counting')],
      mergeRequests=False))

c['builders'].append(
    BuilderConfig(name="all",
      slavenames=["buildslave"],
      factory=factory, locks=[build_lock.access('counting')],
      mergeRequests=False))

####### STATUS TARGETS

# 'status' is a list of Status Targets. The results of each build will be
# pushed to these targets. buildbot/status/*.py has a variety to choose from,
# including web pages, email senders, and IRC bots.

c['status'] = []

from buildbot.status import html
from buildbot.status.web import authz, auth

authz_cfg=authz.Authz(
    # change any of these to True to enable; see the manual for more
    # options
    auth=auth.BasicAuth([("buildmaster","buildmaster_password")]),
    gracefulShutdown = False,
    forceBuild = 'auth', # use this to test your slave once it is set up
    forceAllBuilds = False,
    pingBuilder = False,
    stopBuild = False,
    stopAllBuilds = False,
    cancelPendingBuild = False,
)
c['status'].append(html.WebStatus(http_port=8080, authz=authz_cfg,
                                    change_hook_dialects={'base':True}))
c['status'].append(html.WebStatus(http_port=8010, authz=authz_cfg))

####### PROJECT IDENTITY

# the 'title' string will appear at the top of this buildbot
# installation's html.WebStatus home page (linked to the
# 'titleURL') and is embedded in the title of the waterfall HTML page.

c['title'] = "Taskotron"
c['titleURL'] = "http://localhost:8010/"

# the 'buildbotURL' string should point to the location where the buildbot's
# internal web server (usually the html.WebStatus page) is visible. This
# typically uses the port number set in the Waterfall 'status' entry, but
# with an externally-visible host name which the buildbot cannot figure out
# without some help.

c['buildbotURL'] = "http://localhost:8010/"

####### DB URL

c['db'] = {
    # This specifies what database buildbot uses to store its state.  You can leave
    # this at its default for all but the largest installations.
    #'db_url' : "postgresql://buildmaster:buildmaster_password@127.0.0.1/buildmaster", #TODO: fix this
    'db_url' : "sqlite:///state.sqlite", #TODO: fix this
}
