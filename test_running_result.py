import requests
import json
import uuid

properties = {'item': 'xchat-2.8.8-21.fc20', 'item_type': 'koji_build', 'taskname': 'rpmlint', 'arch': 'x86_64', 'uuid': str(uuid.uuid4())}

data={'author': 'taskotron',
        'project': 'rpmcheck',
        'category': 'x86_64',
        'repository': '',
        'comments': 'build request from taskotron-trigger',
        'properties': json.dumps(properties)}

r = requests.post('http://localhost:8080/change_hook', data=data)
print r
